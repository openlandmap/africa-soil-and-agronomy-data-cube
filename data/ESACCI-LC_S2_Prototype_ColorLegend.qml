<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.18.3" minimumScale="inf" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="0" classificationMax="10" classificationMinMaxOrigin="CumulativeCutFullExtentEstimated" band="1" classificationMin="1" type="singlebandpseudocolor">
      <rasterTransparency/>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0">
          <item alpha="255" value="0" label="No data" color="#000000"/>
          <item alpha="255" value="1" label="Trees cover areas" color="#00a000"/>
          <item alpha="255" value="2" label="Shrubs cover areas" color="#966400"/>
          <item alpha="255" value="3" label="Grassland" color="#ffb400"/>
          <item alpha="255" value="4" label="Cropland" color="#ffff64"/>
          <item alpha="255" value="5" label="Vegetation aquatic or regularly flooded" color="#00dc82"/>
          <item alpha="255" value="6" label="Lichen Mosses / Sparse vegetation" color="#ffebaf"/>
          <item alpha="255" value="7" label="Bare areas" color="#fff5d7"/>
          <item alpha="255" value="8" label="Built up areas" color="#c31400"/>
          <item alpha="255" value="9" label="Snow and/or Ice" color="#ffffff"/>
          <item alpha="255" value="10" label="Open water" color="#0046c8"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
